const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");

const userRoutes = require("./routes/userRoutes");
const productRoutes = require("./routes/productRoutes");

const port = process.env.PORT || 3000;

const app = express();

// Connect to our Mongoose Database
mongoose.connect("mongodb+srv://admin:admin123@batch204-likristoffer.rqyug4v.mongodb.net/s42-s46?retryWrites=true&w=majority", {

	useNewUrlParser: true,
	useUnifiedTopology: true
});

let db = mongoose.connection;
db.on("error", () => console.error.bind(console, "Error"));
db.once("open", () => console.log("Now connected to MongoDB Atlas!"));

// Allows all resources to access our backend application
app.use(cors());
app.use(express.json());

// localhost:4000/users/checkEmail
app.use("/users", userRoutes);
app.use("/products", productRoutes);

app.listen(port, () => {
	console.log(`API is now online on port ${port}`);
})